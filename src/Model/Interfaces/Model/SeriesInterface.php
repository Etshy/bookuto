<?php declare(strict_types=1);


namespace App\Model\Interfaces\Model;

use App\Model\Interfaces\Model\Files\ImageInterface;
use DateTime;
use Doctrine\Common\Collections\Collection;

/**
 * Interface SeriesInterface
 * @package App\Model\Interfaces\Model
 */
interface SeriesInterface extends BaseModelInterface
{
    const ITEMS_NUMBER_PER_PAGE = 15;

    const READER_MODE_CLASSIC_VALUE = 'classic';
    const READER_MODE_FLIPBOOK_VALUE = 'flipbook';
    const READER_MODE_WEBTOON_VALUE = 'webtoon';

    const READER_DIRECTION_LTR_VALUE = 'ltr';
    const READER_DIRECTION_RTL_VALUE = 'rtl';

    public function getName(): string;

    public function setName(string $name): void;

    public function getAdditionalNames(): ?string;

    public function setAdditionalNames(?string $additionalNames): void;

    public function getSlug(): ?string;

    public function setSlug(?string $slug): void;

    public function getImage(): ?ImageInterface;

    public function setImage(?ImageInterface $image): void;

    public function getAuthor(): ?string;

    public function setAuthor(?string $author): void;

    public function getArtist(): ?string;

    public function setArtist(?string $artist): void;

    public function getDescription(): ?string;

    public function setDescription(?string $description): void;

    public function isAdult(): bool;

    public function setAdult(bool $adult): void;

    public function isVisible(): bool;

    public function setVisible(bool $visible): void;

    public function getCustomTitle(): ?string;

    public function setCustomTitle(?string $customTitle): void;

    public function getChapters(): Collection;
    //public function setChapters(Collection $chapters): void;

    public function getTeams(): Collection;

    public function setTeams(Collection $teams): void;

    public function getReaderSettings(): ReaderSettingsInterface;

    public function setReaderSettings(ReaderSettingsInterface $readerSettings): void;

    public function getLastChapterPublishedAt(): ?DateTime;

    public function setLastChapterPublishedAt(?DateTime $lastChapterPublishedAt): void;

    public function toArray(): array;
}
