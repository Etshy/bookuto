<?php declare(strict_types=1);


namespace App\Model\Interfaces\Model;

/**
 * Interface FollowInterface
 * @package App\Model\Interfaces\Model
 */
interface FollowInterface extends BaseModelInterface
{
    const ITEMS_NUMBER_PER_PAGE = 10;

    public function getUser(): ?UserInterface;

    public function setUser(?UserInterface $user): void;

    public function getSeries(): ?SeriesInterface;

    public function setSeries(?SeriesInterface $series): void;
}
