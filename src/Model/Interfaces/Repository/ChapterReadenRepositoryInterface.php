<?php


namespace App\Model\Interfaces\Repository;

use Pagerfanta\Pagerfanta;

/**
 * Interfaces ChapterReadenRepositoryInterface
 * @package App\Model\Interfaces\Repository
 */
interface ChapterReadenRepositoryInterface extends RepositoryInterface
{
    public function getPagination(array $criteria, int $page): Pagerfanta;
}
