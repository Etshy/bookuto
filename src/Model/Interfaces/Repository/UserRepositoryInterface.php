<?php declare(strict_types=1);


namespace App\Model\Interfaces\Repository;

use Pagerfanta\Pagerfanta;

/**
 * Interface UserRepositoryInterface
 * @package App\Model\Interfaces\Repository
 */
interface UserRepositoryInterface extends RepositoryInterface
{
    public function findByCriteriaPaginated(array $criteria, int $page): Pagerfanta;
}
