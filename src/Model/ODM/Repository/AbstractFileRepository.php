<?php
declare(strict_types=1);


namespace App\Model\ODM\Repository;

use App\Exceptions\Services\File\RealPathRequiredException;
use App\Model\Interfaces\Model\Files\FileInterface;
use App\Model\Interfaces\Repository\FileRepositoryInterface;
use App\Model\Interfaces\Repository\RepositoryInterface;
use Doctrine\Bundle\MongoDBBundle\Repository\ServiceGridFSRepository;
use Doctrine\ODM\MongoDB\DocumentManager;
use Doctrine\ODM\MongoDB\DocumentNotFoundException;
use Doctrine\ODM\MongoDB\MongoDBException;
use Doctrine\ODM\MongoDB\Repository\UploadOptions;
use JetBrains\PhpStorm\Pure;

/**
 * Class AbstractFileRepository
 * @package App\Model\ODM\Repository
 */
abstract class AbstractFileRepository extends ServiceGridFSRepository implements RepositoryInterface, FileRepositoryInterface
{
    #[Pure]
    public function getObjectManager(): DocumentManager
    {
        return $this->getDocumentManager();
    }

    /**
     * @throws MongoDBException
     */
    public function batchRemove(array $ids): void
    {
        $qb = $this->createQueryBuilder();
        $qb->remove()
            ->field('id')
            ->in($ids)
            ->getQuery()
            ->execute();
    }

    /**
     * @throws MongoDBException
     * @throws RealPathRequiredException
     */
    public function save(FileInterface $file): object
    {
        if (is_null($file->getRealPath())) {
            throw new RealPathRequiredException('can\'t save file without a realPath');
        }

        $uploadOptions = new UploadOptions();
        $uploadOptions->metadata = $file->getMetadata();

        return $this->uploadFromFile($file->getRealPath(), $file->getName(), $uploadOptions);
    }

    /**
     * @return resource
     * @throws DocumentNotFoundException
     */
    public function getFileStream(FileInterface $file)
    {
        return $this->openDownloadStream($file);
    }

    /**
     * @throws DocumentNotFoundException
     */
    public function setFileInFS(FileInterface $file)
    {
        $stream = fopen($file->getRealPath(), 'wb+');
        $this->downloadToStream($file->getId(), $stream);
        fclose($stream);
    }


}
