<?php
declare(strict_types=1);

namespace App\Model\ODM\Repository;

use App\Model\Criteria\SeriesCriteria;
use App\Model\Interfaces\Model\SeriesInterface;
use App\Model\Interfaces\Repository\SeriesRepositoryInterface;
use App\Model\Persistence\Series;
use Doctrine\Bundle\MongoDBBundle\ManagerRegistry;
use Doctrine\ODM\MongoDB\MongoDBException;
use Doctrine\ODM\MongoDB\Query\Builder;
use Pagerfanta\Doctrine\MongoDBODM\QueryAdapter;
use Pagerfanta\Pagerfanta;

/**
 * Class SeriesRepository
 * @package App\Model\Repository
 */
class SeriesRepository extends BaseRepository implements SeriesRepositoryInterface
{
    public function __construct(ManagerRegistry $managerRegistry)
    {
        parent::__construct($managerRegistry, Series::class);
    }

    public function findByCriteriaPaginated(SeriesCriteria $criteria, int $page = 1): Pagerfanta
    {
        $qb = $this->createQueryBuilder();

        $qb = $this->buildCriteria($qb, $criteria);

        $qb->sort('createdAt', self::SORT_DESC);

        return $this->createPaginator($qb, $page);
    }

    /**
     * @return SeriesInterface[]
     * @throws MongoDBException
     */
    public function findByCriteria(SeriesCriteria $criteria): array
    {
        $qb = $this->createQueryBuilder();

        $qb = $this->buildCriteria($qb, $criteria);

        return $qb->getQuery()->execute();
    }

    private function buildCriteria(Builder $qb, SeriesCriteria $criteria): Builder
    {
        if (!is_null($criteria->getVisible())) {
            $qb->field('visible')->equals($criteria->getVisible());
        }
        if ($criteria->getExcludeTags()) {
            $qb->field('tags')->notIn($criteria->getExcludeTags());
        }
        if ($criteria->getIncludeTags()) {
            if ($criteria->getInclusionMode() === SeriesCriteria::INCLUSION_MODE_ANY) {
                $qb->field('tags')->in($criteria->getIncludeTags());
            } else {
                $qb->field('tags')->all($criteria->getIncludeTags());
            }
        }
        if ($criteria->getStatus()) {
            $qb->field('status')->equals($criteria->getStatus());
        }
        if ($criteria->getAuthorOrArtist()) {
            $qb->addOr(
                $qb->expr()->field('author')->equals($criteria->getAuthorOrArtist()),
                $qb->expr()->field('artist')->equals($criteria->getAuthorOrArtist()),
            );
        }

        return $qb;
    }

    private function createPaginator(Builder $query, int $page): Pagerfanta
    {
        $paginator = new Pagerfanta(new QueryAdapter($query));
        $paginator->setMaxPerPage(SeriesInterface::ITEMS_NUMBER_PER_PAGE);
        $paginator->setCurrentPage($page);

        return $paginator;
    }
}
