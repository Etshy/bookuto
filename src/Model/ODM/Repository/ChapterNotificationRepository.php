<?php
declare(strict_types=1);


namespace App\Model\ODM\Repository;

use App\Model\Interfaces\Repository\ChapterNotificationRepositoryInterface;

/**
 * Class ChapterNotificationRepository
 * @package App\Model\ODM\Repository
 */
class ChapterNotificationRepository extends NotificationRepository implements ChapterNotificationRepositoryInterface
{
}
