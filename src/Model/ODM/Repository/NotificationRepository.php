<?php
declare(strict_types=1);

namespace App\Model\ODM\Repository;

use App\Model\Interfaces\Model\NotificationInterface;
use App\Model\Interfaces\Model\SeriesInterface;
use App\Model\Interfaces\Model\UserInterface;
use App\Model\Interfaces\Repository\NotificationRepositoryInterface;
use App\Model\Persistence\Notification;
use Doctrine\ODM\MongoDB\Iterator\Iterator;
use Doctrine\ODM\MongoDB\MongoDBException;
use Doctrine\ODM\MongoDB\Query\Builder;
use Doctrine\Persistence\ManagerRegistry;
use Pagerfanta\Doctrine\MongoDBODM\QueryAdapter;
use Pagerfanta\Pagerfanta;

/**
 * Class NotificationRepository
 * @package App\Model\ODM\Repository
 *
 */
class NotificationRepository extends BaseRepository implements NotificationRepositoryInterface
{
    public function __construct(ManagerRegistry $managerRegistry)
    {
        parent::__construct($managerRegistry, Notification::class);
    }

    /**
     * @throws MongoDBException
     */
    public function findByCriteria(array $criteria): Iterator
    {
        $builder = $this->createQueryBuilder();

        $builder = $this->buildCriteria($builder, $criteria);

        return $builder->getQuery()->getIterator();
    }

    public function findOneByCriteria(array $criteria): ?NotificationInterface
    {
        $builder = $this->createQueryBuilder();

        $builder = $this->buildCriteria($builder, $criteria);

        $notification = $builder->getQuery()->getSingleResult();
        if (!$notification instanceof NotificationInterface) {
            return null;
        }

        return $notification;
    }

    public function getPagination(array $criteria, int $page): Pagerfanta
    {
        $qb = $this->createQueryBuilder();

        $qb = $this->buildCriteria($qb, $criteria);

        $qb->sort('createdAt', BaseRepository::SORT_DESC);

        return $this->createPaginator($qb, $page);
    }

    /**
     * @throws MongoDBException
     */
    public function countUnread(array $criteria): int
    {
        $qb = $this->createQueryBuilder();
        $qb->count();
        $qb = $this->buildCriteria($qb, $criteria);

        return $qb->getQuery()->execute();
    }

    /**
     * @throws MongoDBException
     */
    public function setAllAsSentForUser(UserInterface $user): void
    {
        $qb = $this->createQueryBuilder();

        $qb->updateMany()
            ->field('sent')->set(true)
            ->field('user')->references($user)
            ->field('sent')->equals(false);

        $qb->getQuery()->execute();
    }

    private function buildCriteria(Builder $builder, array $criteria): Builder
    {
        if (isset($criteria['user'])) {
            if ($criteria['user'] instanceof UserInterface) {
                $builder->field('user')->references($criteria['user']);
            }
        }

        if (isset($criteria['series'])) {
            if ($criteria['series'] instanceof SeriesInterface) {
                $builder->field('series')->references($criteria['series']);
            }
        }

        if (isset($criteria['readen']) && is_bool($criteria['readen'])) {
            $builder->field('readen')->equals($criteria['readen']);
        }

        if (isset($criteria['sent']) && is_bool($criteria['sent'])) {
            $builder->field('sent')->equals($criteria['sent']);
        }

        if (isset($criteria['limit']) && is_int($criteria['limit'])) {
            $builder->limit($criteria['limit']);
        }

        if (isset($criteria['orderby']) && is_array($criteria['orderby'])) {
            $builder->sort($criteria['orderby']['fieldname'], $criteria['orderby']['order']);
        }

        return $builder;
    }

    private function createPaginator(Builder $query, int $page = 1): Pagerfanta
    {
        $paginator = new Pagerfanta(new QueryAdapter($query));
        $paginator->setMaxPerPage(NotificationInterface::ITEMS_NUMBER_PER_PAGE);
        $paginator->setCurrentPage($page);

        return $paginator;
    }
}
