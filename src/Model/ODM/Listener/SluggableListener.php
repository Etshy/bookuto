<?php
declare(strict_types=1);


namespace App\Model\ODM\Listener;

use App\Model\Interfaces\Model\SluggableInterface;
use Doctrine\Common\EventSubscriber;
use Doctrine\ODM\MongoDB\Event\LifecycleEventArgs;
use Doctrine\ODM\MongoDB\Events;
use Symfony\Component\String\Slugger\SluggerInterface;

/**
 * Class SluggableListener
 * @package App\Model\ODM\Listener
 */
class SluggableListener implements EventSubscriber
{
    protected SluggerInterface $slugger;

    public function __construct(SluggerInterface $slugger)
    {
        $this->slugger = $slugger;
    }


    public function getSubscribedEvents(): array
    {
        return [
            Events::prePersist,
            Events::preUpdate,
        ];
    }

    public function prePersist(LifecycleEventArgs $eventArgs): void
    {
        $document = $eventArgs->getDocument();
        if ($document instanceof SluggableInterface) {
            $document->setSlug($this->slugger->slug($document->getName())->lower()->toString());
        }
    }

    public function preUpdate(LifecycleEventArgs $eventArgs): void
    {
        $document = $eventArgs->getDocument();
        if ($document instanceof SluggableInterface) {
            $document->setSlug($this->slugger->slug($document->getName())->lower()->toString());

            $class = $eventArgs->getDocumentManager()->getClassMetadata(get_class($document));
            $eventArgs->getDocumentManager()->getUnitOfWork()->recomputeSingleDocumentChangeSet($class, $document);
        }
    }
}
