<?php
declare(strict_types=1);

namespace App\Model\Persistence;

use App\Model\Interfaces\Model\BaseModelInterface;
use DateTime;

/**
 * Class BaseModel
 * @package App\Model\Persistence
 */
abstract class BaseModel implements BaseModelInterface
{
    protected string|int|null $id = null;
    protected DateTime $createdAt;
    protected ?DateTime $updatedAt = null;

    public function getId(): string|int|null
    {
        return $this->id;
    }

    public function setId(string|int $id): void
    {
        $this->id = $id;
    }

    public function getCreatedAt(): ?DateTime
    {
        return $this->createdAt;
    }

    public function setCreatedAt(DateTime $createdAt): void
    {
        $this->createdAt = $createdAt;
    }

    public function getUpdatedAt(): ?DateTime
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(?DateTime $updatedAt): void
    {
        $this->updatedAt = $updatedAt;
    }
}
