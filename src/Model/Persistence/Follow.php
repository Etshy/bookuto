<?php
declare(strict_types=1);

namespace App\Model\Persistence;

use App\Model\Interfaces\Model\FollowInterface;
use App\Model\Interfaces\Model\SeriesInterface;
use App\Model\Interfaces\Model\UserInterface;

/**
 * Class Follow
 * @package App\Model\Persistence
 */
class Follow extends BaseModel implements FollowInterface
{
    protected ?UserInterface $user;
    protected ?SeriesInterface $series;

    public function getUser(): ?UserInterface
    {
        return $this->user;
    }

    public function setUser(?UserInterface $user): void
    {
        $this->user = $user;
    }

    public function getSeries(): ?SeriesInterface
    {
        return $this->series;
    }

    public function setSeries(?SeriesInterface $series): void
    {
        $this->series = $series;
    }
}
