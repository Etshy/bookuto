<?php
declare(strict_types=1);

namespace App\Model\Persistence\Files;

use App\Model\Interfaces\Model\Files\ImageInterface;

/**
 * Class Image
 * @package App\Model\Persistence\Files
 */
class Image extends File implements ImageInterface
{

}
