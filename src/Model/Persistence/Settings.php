<?php
declare(strict_types=1);

namespace App\Model\Persistence;

use App\Model\Interfaces\Model\Files\ImageInterface;
use App\Model\Interfaces\Model\SettingsInterface;
use JetBrains\PhpStorm\ArrayShape;
use JsonSerializable;

/**
 * Class Settings
 * @package App\Model\Persistence
 */
class Settings extends BaseModel implements SettingsInterface, JsonSerializable
{
    protected ?string $defaultReaderMode = null;
    protected ?string $defaultReaderDirection = null;
    protected ?bool $downloadEnabled = false;
    protected ?bool $downloadProtected = false;
    protected ?ImageInterface $logo = null;
    protected bool $imageScrambled = false;

    public function __construct()
    {
        $this->imageScrambled = false;
    }

    public function getDefaultReaderMode(): ?string
    {
        return $this->defaultReaderMode;
    }

    public function setDefaultReaderMode(?string $defaultReaderMode): void
    {
        $this->defaultReaderMode = $defaultReaderMode;
    }

    public function getDefaultReaderDirection(): ?string
    {
        return $this->defaultReaderDirection;
    }

    public function setDefaultReaderDirection(?string $defaultReaderDirection): void
    {
        $this->defaultReaderDirection = $defaultReaderDirection;
    }

    public function getLogo(): ?ImageInterface
    {
        return $this->logo;
    }

    public function setLogo(?ImageInterface $logo): void
    {
        $this->logo = $logo;
    }

    /**
     * Specify data which should be serialized to JSON
     * @link https://php.net/manual/en/jsonserializable.jsonserialize.php
     * @return mixed data which can be serialized by <b>json_encode</b>,
     * which is a value of any type other than a resource.
     * @since 5.4
     */
    #[ArrayShape(['imageScrambled' => "bool", 'downloadEnabled' => "bool"])]
    public function jsonSerialize(): array
    {
        return $this->toArray();
    }

    #[ArrayShape(['imageScrambled' => "bool", 'downloadEnabled' => "bool"])]
    public function toArray(): array
    {
        return [
            'imageScrambled' => $this->isImageScrambled(),
            'downloadEnabled' => $this->isDownloadEnabled(),
        ];
    }

    public function isImageScrambled(): bool
    {
        return $this->imageScrambled;
    }

    public function setImageScrambled(bool $imageScrambled): void
    {
        $this->imageScrambled = $imageScrambled;
    }

    public function isDownloadEnabled(): bool
    {
        return $this->downloadEnabled ?? false;
    }

    public function setDownloadEnabled(?bool $downloadEnabled): void
    {
        $this->downloadEnabled = $downloadEnabled;
    }

    public function isDownloadProtected(): bool
    {
        return $this->downloadProtected ?? false;
    }

    public function setDownloadProtected(?bool $downloadProtected): void
    {
        $this->downloadProtected = $downloadProtected;
    }
}
