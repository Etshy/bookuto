<?php
declare(strict_types=1);

namespace App\Voter;

use App\Model\Interfaces\Model\TeamInterface;
use App\Model\Interfaces\Model\UserInterface;
use App\Utils\Traits\ConstantsTrait;
use LogicException;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\AccessDecisionManagerInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;

/**
 * Class TeamVoter
 * @package App\Voter
 */
class TeamVoter extends Voter
{
    use ConstantsTrait;

    //Teams Roles
    public const ROLE_LIST_TEAMS = 'ROLE_LIST_TEAMS';
    public const ROLE_ADD_TEAM = 'ROLE_ADD_TEAM';
    public const ROLE_VIEW_TEAM = 'ROLE_VIEW_TEAM';
    public const ROLE_EDIT_TEAM = 'ROLE_EDIT_TEAM';
    public const ROLE_DELETE_TEAM = 'ROLE_DELETE_TEAM';

    private AccessDecisionManagerInterface $decisionManager;

    public function __construct(AccessDecisionManagerInterface $decisionManager)
    {
        $this->decisionManager = $decisionManager;
    }

    /**
     * Determines if the attribute and subject are supported by this voter.
     *
     * @param string $attribute An attribute
     * @param mixed $subject The subject to secure, e.g. an object the user wants to access or any other PHP type
     *
     * @return bool True if the attribute and subject are supported, false otherwise
     */
    protected function supports(string $attribute, $subject): bool
    {
        //$constants = $this->getConstants();
        $constants = [
            self::ROLE_LIST_TEAMS,
            self::ROLE_ADD_TEAM,
            self::ROLE_VIEW_TEAM,
            self::ROLE_EDIT_TEAM,
            self::ROLE_DELETE_TEAM,
        ];

        // if the attribute isn't one we support, return false
        if (!in_array($attribute, $constants)) {
            return false;
        }

        //For add and list perms, we don't have a series to control, so we bypass that.
        if (!in_array($attribute, [self::ROLE_ADD_TEAM, self::ROLE_LIST_TEAMS]) && !$subject instanceof TeamInterface) {
            return false;
        }

        return true;
    }

    /**
     * Perform a single access check operation on a given attribute, subject and token.
     * It is safe to assume that $attribute and $subject already passed the "supports()" method check.
     *
     * @param string $attribute
     * @param mixed $subject
     * @param TokenInterface $token
     *
     * @return bool
     */
    protected function voteOnAttribute(string $attribute, $subject, TokenInterface $token): bool
    {
        //ADMIN and SUPER_ADMIN can do anything they want !
        if ($this->decisionManager->decide($token, [UserVoter::ROLE_ADMIN])) {
            return true;
        }

        $user = $token->getUser();
        if (!$user instanceof UserInterface) {
            // the user must be logged in. if not, deny access
            return false;
        }

        $team = $subject;
        switch ($attribute) {
            case self::ROLE_DELETE_TEAM:
            case self::ROLE_ADD_TEAM:
            case self::ROLE_LIST_TEAMS:
                if (in_array($attribute, $user->getRoles())) {
                    return true;
                }
                break;

            case self::ROLE_VIEW_TEAM:
                if (in_array($attribute, $user->getRoles())) {
                    return $this->canView($team, $user);
                }
                break;

            case self::ROLE_EDIT_TEAM:
                if (in_array($attribute, $user->getRoles())) {
                    return $this->canEdit($team, $user);
                }
                break;

            default:
                throw new LogicException('This code should not be reached!');
        }

        return false;
    }

    private function canView(TeamInterface $team, UserInterface $user): bool
    {
        if ($team instanceof TeamInterface) {
            //if user can edit, he can view.
            return $this->canEdit($team, $user);
        }

        return false;
    }

    private function canEdit(TeamInterface $team, UserInterface $user): bool
    {
        if ($team instanceof TeamInterface) {
            //if user can edit, he can view.
            return $this->userIsManagerOfTeam($team, $user);
        }

        return false;
    }

    private function userIsManagerOfTeam(TeamInterface $team, UserInterface $user): bool
    {
        //We search in the Managers of the team
        $members = $team->getManagers();

        foreach ($members as $member) {
            if (!$member instanceof UserInterface) {
                continue;
            }
            //if the current User is in the managers, he can update it
            if ($member->getId() === $user->getId()) {
                return true;
            }
        }

        return false;
    }
}
