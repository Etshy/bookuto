<?php
declare(strict_types=1);

namespace App\Voter;

use App\Model\Interfaces\Model\UserInterface;
use App\Utils\Traits\ConstantsTrait;
use LogicException;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\AccessDecisionManagerInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;

/**
 * Class SettingsVoter
 * @package App\Voter
 */
class SettingsVoter extends Voter
{
    use ConstantsTrait;

    //Settings Roles
    public const ROLE_EDIT_GENERAL_SETTINGS = 'ROLE_EDIT_GENERAL_SETTINGS';
    public const ROLE_EDIT_READER_SETTINGS = 'ROLE_EDIT_READER_SETTINGS';

    private AccessDecisionManagerInterface $decisionManager;

    public function __construct(AccessDecisionManagerInterface $decisionManager)
    {
        $this->decisionManager = $decisionManager;
    }

    /**
     * Determines if the attribute and subject are supported by this voter.
     *
     * @param string $attribute An attribute
     * @param mixed $subject The subject to secure, e.g. an object the user wants to access or any other PHP type
     *
     * @return bool True if the attribute and subject are supported, false otherwise
     */
    protected function supports(string $attribute, $subject): bool
    {
        //$constants = $this->getConstants();
        $constants = [
            self::ROLE_EDIT_READER_SETTINGS,
            self::ROLE_EDIT_GENERAL_SETTINGS,
        ];

        // if the attribute isn't one we support, return false
        if (!in_array($attribute, $constants)) {
            return false;
        }

        return true;
    }

    /**
     * Perform a single access check operation on a given attribute, subject and token.
     * It is safe to assume that $attribute and $subject already passed the "supports()" method check.
     *
     * @param string $attribute
     * @param mixed $subject
     * @param TokenInterface $token
     *
     * @return bool
     */
    protected function voteOnAttribute(string $attribute, $subject, TokenInterface $token): bool
    {
        //ADMIN and SUPER_ADMIN can do anything they want !
        if ($this->decisionManager->decide($token, [UserVoter::ROLE_ADMIN])) {
            return true;
        }

        $user = $token->getUser();
        if (!$user instanceof UserInterface) {
            // the user must be logged in. if not, deny access
            return false;
        }

        switch ($attribute) {
            case self::ROLE_EDIT_READER_SETTINGS:
            case self::ROLE_EDIT_GENERAL_SETTINGS:
                if (in_array($attribute, $user->getRoles())) {
                    return true;
                }
                break;

            default:
                throw new LogicException('This code should not be reached!');
        }

        return false;
    }
}
