<?php
declare(strict_types=1);

namespace App\Form\Backend\Profile;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Security\Core\Validator\Constraints\UserPassword;
use Symfony\Component\Validator\Constraints\NotCompromisedPassword;
use Symfony\Contracts\Translation\TranslatorInterface;

/**
 * Class ChangePasswordForm
 * @package App\Form\Backend\Profile
 */
class ChangePasswordForm extends AbstractType
{

    public const CURRENT_PASSWORD = 'current_password';
    public const PLAIN_PASSWORD = 'plainPassword';
    protected TranslatorInterface $translator;

    public function __construct(TranslatorInterface $translator)
    {
        $this->translator = $translator;
    }


    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add(self::CURRENT_PASSWORD, PasswordType::class, [
                'constraints' => [
                    new UserPassword(),
                ],
                'label' => $this->translator->trans('label.current_password'),
            ])
            ->add(self::PLAIN_PASSWORD, PasswordType::class, [
                'constraints' => [
                    new NotCompromisedPassword(),
                ],
                'label' => $this->translator->trans('label.new_password'),
            ]);
    }

}
