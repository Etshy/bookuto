<?php
declare(strict_types=1);

namespace App\Form\Backend\Settings;

use App\Model\Interfaces\Model\SeriesInterface;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\FormBuilderInterface;

/**
 * Class ReaderSettingsForm
 * @package App\Form\Backend\Settings
 */
class ReaderSettingsForm extends AbstractType
{
    public const DEFAULT_READER_MODE = 'defaultReaderMode';
    public const DEFAULT_READER_DIRECTION = 'defaultReaderDirection';
    public const IMAGE_SCRAMBLED = 'imageScrambled';
    public const DOWNLOAD_ENABLED = 'downloadEnabled';
    public const DOWNLOAD_PROTECTED = 'downloadProtected';

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder->add('id', HiddenType::class)
            ->add(self::DEFAULT_READER_MODE, ChoiceType::class, [
                'choices' => [
                    'label.reader_mode.classic' => SeriesInterface::READER_MODE_CLASSIC_VALUE,
                    'label.reader_mode.webtoon' => SeriesInterface::READER_MODE_WEBTOON_VALUE,
                ],
                'required' => false,
                'placeholder' => 'label.reader_mode.choose',
                'label' => 'label.reader_mode.label',
                'attr' => [
                    'class' => 'ui inverted dropdown',
                ],
            ])
            ->add(self::DEFAULT_READER_DIRECTION, ChoiceType::class, [
                'choices' => [
                    'label.reader_direction.ltr' => SeriesInterface::READER_DIRECTION_LTR_VALUE,
                    'label.reader_direction.rtl' => SeriesInterface::READER_DIRECTION_RTL_VALUE,
                ],
                'required' => false,
                'placeholder' => 'label.reader_direction.choose',
                'label' => 'label.reader_direction.label',
                'attr' => [
                    'class' => 'ui inverted dropdown',
                ],
            ])
            ->add(self::IMAGE_SCRAMBLED, CheckboxType::class, [
                'required' => false,
                'label' => 'label.activate_image_scramble',
            ])
            ->add(self::DOWNLOAD_ENABLED, CheckboxType::class, [
                'required' => false,
                'label' => 'label.download.activate',
            ])
            ->add(self::DOWNLOAD_PROTECTED, CheckboxType::class, [
                'required' => false,
                'label' => 'label.download.protect',
            ]);
    }
}
