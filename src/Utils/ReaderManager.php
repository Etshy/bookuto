<?php
declare(strict_types=1);


namespace App\Utils;

use App\Form\Frontend\ReaderSettingsForm;
use App\Model\Interfaces\Model\SeriesInterface;
use App\Model\Interfaces\Model\UserInterface;
use JetBrains\PhpStorm\ArrayShape;
use SunCat\MobileDetectBundle\DeviceDetector\MobileDetector;
use Symfony\Component\HttpFoundation\Cookie;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class ReaderManager
 * @package App\Utils
 */
class ReaderManager
{
    public const COOKIE_READER_MODE = 'COOKIE_READER_MODE';
    public const COOKIE_READER_DIRECTION = 'COOKIE_READER_DIRECTION';
    public const COOKIE_CLEAR = 'COOKIE_CLEAR';

    public function __construct()
    {
    }


    /**
     * @param Request $request
     * @param UserInterface|null $user
     * @param SeriesInterface $series
     *
     * @return array
     */
    #[ArrayShape([ReaderSettingsForm::READER_DIRECTION => "mixed", ReaderSettingsForm::READER_MODE => "mixed"])]
    public function getReaderPreferences(Request $request, ?UserInterface $user, SeriesInterface $series): array
    {
        $readerMode = null;
        $readerDirection = null;

        /*if ($this->mobileDetector->isMobile()) {
            //Mobile device, we force webtoon view
            $readerMode = SeriesInterface::READER_MODE_WEBTOON_VALUE;
        }*/

        if ($user instanceof UserInterface) {
            $readerMode = $user->getSettings()?->getReaderMode();
            $readerDirection = $user->getSettings()?->getReaderDirection();
        }

        if (is_null($readerMode) && $request->cookies->has(self::COOKIE_READER_MODE)) {
            //no user preference and cookie exists
            $readerMode = $request->cookies->get(self::COOKIE_READER_MODE);
        }
        if (is_null($readerMode) && $series->getReaderSettings()->getReaderMode()) {
            //no readerMode yet
            $readerMode = $series->getReaderSettings()->getReaderMode();
        }
        if (is_null($readerMode)) {
            $readerMode = SeriesInterface::READER_MODE_CLASSIC_VALUE;
        }

        if (is_null($readerDirection) && $request->cookies->has(self::COOKIE_READER_DIRECTION)) {
            //no user preference and cookie exists
            $readerDirection = $request->cookies->get(self::COOKIE_READER_DIRECTION);
        }
        if (is_null($readerDirection) && $series->getReaderSettings()->getReaderDirection()) {
            //no readerMode yet
            $readerDirection = $series->getReaderSettings()->getReaderDirection();
        }
        if (is_null($readerDirection)) {
            $readerDirection = SeriesInterface::READER_DIRECTION_RTL_VALUE;
        }

        return [
            ReaderSettingsForm::READER_DIRECTION => $readerDirection,
            ReaderSettingsForm::READER_MODE => $readerMode,
        ];
    }

    /**
     * @param JsonResponse $jsonResponse
     * @param array $data
     *
     * @return JsonResponse
     */
    public function setCookieFromData(JsonResponse $jsonResponse, array $data): JsonResponse
    {
        $jsonResponse->headers->setCookie(new Cookie(self::COOKIE_READER_MODE, $data[ReaderSettingsForm::READER_MODE]));
        $jsonResponse->headers->setCookie(new Cookie(self::COOKIE_READER_DIRECTION, $data[ReaderSettingsForm::READER_DIRECTION]));

        return $jsonResponse;
    }
}
