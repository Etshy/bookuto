<?php declare(strict_types=1);

namespace App\Utils\AppUpdater;

use RuntimeException;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;

class Downloader
{
    protected HttpClientInterface $httpClient;
    protected string $rootPath;

    public function __construct(
        string $rootPath,
        HttpClientInterface $httpClient,
    ) {

        $this->httpClient = $httpClient;
        $this->rootPath = $rootPath;
    }

    /**
     * @throws TransportExceptionInterface
     */
    public function downloadFile(string $sourceUrl, string $name): string
    {
        $response = $this->httpClient->request('GET', $sourceUrl);

        // Responses are lazy: this code is executed as soon as headers are received
        if (200 !== $response->getStatusCode()) {
            throw new RuntimeException('...');
        }

        // get the response content in chunks and save them in a file
        // response chunks implement Symfony\Contracts\HttpClient\ChunkInterface
        $fileHandler = fopen($this->rootPath.'/'.$name.'.zip', 'wb');
        foreach ($this->httpClient->stream($response) as $chunk) {
            fwrite($fileHandler, $chunk->getContent());
        }

        //file should be donwloaded now
        return $this->rootPath.'/'.$name.'.zip';
    }
}
