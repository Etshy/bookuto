<?php
declare(strict_types=1);

namespace App\Controller;

use App\Form\RegistrationForm;
use App\Model\Persistence\User;
use App\Security\EmailVerifier;
use App\Security\LoginFormCustomAuthenticator;
use App\Security\SecurityService;
use App\Service\UserService;
use Exception;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Mailer\Exception\TransportExceptionInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Authentication\UserAuthenticatorInterface;
use Symfony\Contracts\Translation\TranslatorInterface;
use SymfonyCasts\Bundle\VerifyEmail\Exception\VerifyEmailExceptionInterface;

/**
 * Class RegistrationController
 * @package App\Controller
 */
class RegistrationController extends AbstractController
{
    protected UserService $userService;
    protected TranslatorInterface $translator;
    protected SecurityService $sercurityService;
    private EmailVerifier $emailVerifier;

    public function __construct(
        EmailVerifier $emailVerifier,
        UserService $userService,
        TranslatorInterface $translator,
        SecurityService $sercurityService
    ) {
        $this->emailVerifier = $emailVerifier;
        $this->userService = $userService;
        $this->translator = $translator;
        $this->sercurityService = $sercurityService;
    }

    /**
     * @throws TransportExceptionInterface
     */
    #[Route('/register', name: 'register')]
    public function register(
        Request $request,
        LoginFormCustomAuthenticator $authenticator,
        UserAuthenticatorInterface $authenticatorManager
    ): Response {

        $user = new User();
        $form = $this->createForm(RegistrationForm::class, $user);

        try {
            $form->handleRequest($request);
            if ($form->isSubmitted() && $form->isValid()) {

                // encode the plain password
                $this->userService->updatePassword($user, $form->get('plainPassword')->getData());
                if (filter_var($form->get('username')->getData(), FILTER_VALIDATE_EMAIL)) {
                    //username is also an email
                    $user->setEmail($user->getUsername());
                }
                $this->userService->updateCanonicalFields($user);

                $this->userService->save($user);

                //if username is an email send verfication
                if (filter_var($form->get('username')->getData(), FILTER_VALIDATE_EMAIL)) {
                    $this->sercurityService->sendConfirmationEmail($user);
                }

                return $authenticatorManager->authenticateUser($user, $authenticator, $request);
            }

        } catch (Exception $e) {
            $i = $e->getCode();
            if ($i === 11000) {
                //Duplication on unique index
                $this->addFlash('error', $this->translator->trans('error.username.already_exists'));
            } else {
                $this->addFlash('error', $this->translator->trans('error.occurred'));
            }
        }

        return $this->render('registration/register.html.twig', [
            'registrationForm' => $form->createView(),
        ]);
    }

    #[Route('/verify/email', name: 'registration_verify_email')]
    public function verifyUserEmail(
        Request $request
    ): Response {
        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY');

        // validate email confirmation link, sets User::isVerified=true and persists
        try {
            $this->emailVerifier->handleEmailConfirmation($request, $this->getUser());
        } catch (VerifyEmailExceptionInterface $exception) {
            $this->addFlash('verify_email_error', $exception->getReason());

            return $this->redirectToRoute('register');
        }

        $this->addFlash('success', $this->translator->trans('email.successfully.verified'));

        return $this->redirectToRoute('homepage');
    }
}
