<?php
declare(strict_types=1);

namespace App\Controller;

use App\Model\Interfaces\Model\Files\FileInterface;
use App\Model\Interfaces\Model\Files\ImageInterface;
use App\Model\Persistence\Files\Image;
use App\Service\FileService;
use App\Service\LocalFileService;
use App\Utils\FileManager;
use Exception;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Translation\TranslatorInterface;

/**
 * Class FileController
 * @package App\Controller
 */
class FileController extends AbstractController
{

    protected FileService $fileService;
    protected LocalFileService $localFileService;
    protected FileManager $fileManager;
    protected TranslatorInterface $translator;

    public function __construct(FileService $fileService, LocalFileService $localFileService, FileManager $fileManager, TranslatorInterface $translator)
    {
        $this->fileService = $fileService;
        $this->localFileService = $localFileService;
        $this->fileManager = $fileManager;
        $this->translator = $translator;
    }

    /**
     * @throws Exception
     */
    #[Route('/render/{id}', name: 'render_image', requirements: ['id' => '\w+'])]
    public function renderImage(
        string $id,
        Request $request
    ): Response {
        $file = $this->searchImage($id);

        $width = ($request->query->getInt('width') ?: null);
        $height = ($request->query->getInt('height') ?: null);
        $maxwidth = ($request->query->getInt('maxwidth') ?: null);
        $maxheight = ($request->query->getInt('maxheight') ?: null);

        if ($file instanceof Image) {
            //If Image from DB , we have to write the file in filesystem
            $this->fileService->downloadToTmpFile($file);
        }

        $simpleImage = $this->fileManager->createSimpleImageFromImage($file);

        //resize the images if needed
        $this->fileManager->resizeImage($simpleImage, $width, $height, $maxwidth, $maxheight);

        if ($file instanceof Image) {
            //If Image from DB, we can delete the image in filesystem
            unlink($file->getRealPath());
        }

        $header = [
            'Content-type' => $file->getMetadata()->getMime(),
        ];
        $response = new Response($simpleImage->toString(), 200, $header);
        $disposition = $response->headers->makeDisposition(
            ResponseHeaderBag::DISPOSITION_INLINE,
            $file->getName()
        );
        $response->headers->set('Content-Disposition', $disposition);

        $response->setCache([
            'etag' => md5($simpleImage->toString()),
            'last_modified' => $file->getUploadDate(),
            'max_age' => 1296000, //15 days
            's_maxage' => 1296000,
            'private' => false,
            'public' => true,
        ]);

        return $response;
    }

    private function searchImage(string $id): FileInterface|ImageInterface
    {
        $file = $this->fileService->find($id);
        if (!$file instanceof ImageInterface) {
            $file = $this->localFileService->find($id);
        }

        if (!$file instanceof ImageInterface) {
            throw $this->createNotFoundException($this->translator->trans('error.not_found.image'));
        }

        return $file;
    }
}
