<?php
declare(strict_types=1);

namespace App\Message;

use App\Message\DTO\ChapterDTO;
use App\Message\DTO\UserDTO;
use App\Message\Interfaces\NotificationInterface;

/**
 * Class ChapterUserNotificationMessage
 * @package App\Message
 */
class ChapterUserNotificationMessage implements NotificationInterface
{
    protected ChapterDTO $chapter;
    protected UserDTO $user;

    public function getChapter(): ChapterDTO
    {
        return $this->chapter;
    }

    public function setChapter(ChapterDTO $chapter): void
    {
        $this->chapter = $chapter;
    }

    public function getUser(): UserDTO
    {
        return $this->user;
    }

    public function setUser(UserDTO $user): void
    {
        $this->user = $user;
    }
}
