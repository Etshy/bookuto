<?php
declare(strict_types=1);

namespace App\Message\Handler;

use App\Mailer\Mailer;
use App\Message\EmailMessage;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;
use Throwable;

/**
 * Class EmailMessageHandler
 * @package App\Message\Handler
 */
class EmailMessageHandler implements MessageHandlerInterface
{

    private Mailer $mailer;

    public function __construct(Mailer $mailer)
    {
        $this->mailer = $mailer;
    }

    public function __invoke(EmailMessage $message): void
    {
        try {
            $mail = $message->getMail();
            if (!$mail->getFromMail()) {
                $mail->setFromMail('no-reply@booku.to');
            }
            $this->mailer->sendMail($mail);
        } catch (Throwable) {
            //TODO log Exception
        }
    }
}
