<?php
declare(strict_types=1);


namespace App\Security;

use App\Model\Interfaces\Model\UserInterface;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Component\Mailer\Exception\TransportExceptionInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

/**
 * Class SercurityService
 * @package App\Security
 */
class SecurityService
{
    /**
     * @var EmailVerifier
     */
    protected EmailVerifier $emailVerifier;
    /**
     * @var TranslatorInterface
     */
    protected TranslatorInterface $translator;

    /**
     * SercurityService constructor.
     *
     * @param EmailVerifier $emailVerifier
     * @param TranslatorInterface $translator
     */
    public function __construct(EmailVerifier $emailVerifier, TranslatorInterface $translator)
    {
        $this->emailVerifier = $emailVerifier;
        $this->translator = $translator;
    }

    /**
     * @param UserInterface $user
     *
     * @throws TransportExceptionInterface
     */
    public function sendConfirmationEmail(UserInterface $user): void
    {
        // generate a signed url and email it to the user
        $this->emailVerifier->sendEmailConfirmation(
            'registration_verify_email',
            $user,
            (new TemplatedEmail())
                ->to($user->getEmail())
                ->subject($this->translator->trans('email.subject.email-confirmation'))
                ->htmlTemplate('registration/confirmation_email.html.twig')
        );
    }
}
