<?php
declare(strict_types=1);

namespace App\Mailer;

/**
 * Class Mail
 * @package App\Mailer
 */
class Mail
{
    private string $subject;

    private string $fromMail;

    private array $toEmail;

    private ?string $plainBody;

    private ?string $htmlBody;

    public function getSubject(): string
    {
        return $this->subject;
    }

    public function setSubject(string $subject): void
    {
        $this->subject = $subject;
    }

    public function getFromMail(): string
    {
        return $this->fromMail;
    }

    public function setFromMail(string $fromMail): void
    {
        $this->fromMail = $fromMail;
    }

    public function getToEmail(): array
    {
        return $this->toEmail;
    }

    public function setToEmail(array $toEmail): void
    {
        $this->toEmail = $toEmail;
    }

    public function getPlainBody(): ?string
    {
        return $this->plainBody;
    }

    public function setPlainBody(?string $plainBody): void
    {
        $this->plainBody = $plainBody;
    }

    public function getHtmlBody(): ?string
    {
        return $this->htmlBody;
    }

    public function setHtmlBody(?string $htmlBody): void
    {
        $this->htmlBody = $htmlBody;
    }
}
