<?php
declare(strict_types=1);

namespace App\Mailer;

use App\Exceptions\Mailer\NoRecipientException;
use Symfony\Component\DependencyInjection\ParameterBag\ContainerBagInterface;
use Symfony\Component\Mailer\Exception\TransportExceptionInterface;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Email;
use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Error\SyntaxError;

/**
 * Class Mailer
 * @package App\Mailer
 */
class Mailer
{
    protected ContainerBagInterface $params;
    protected MailerInterface $mailer;
    private Environment $templating;

    public function __construct(
        Environment $templating,
        ContainerBagInterface $params,
        MailerInterface $mailer
    ) {
        $this->templating = $templating;
        $this->params = $params;
        $this->mailer = $mailer;
    }


    /**
     * Render a template from $template name with $parameters and set the htmlBody of $mail
     *
     * @param Mail $mail
     * @param string $template
     * @param array|null $parameters
     *
     * @return Mail
     * @throws LoaderError
     * @throws RuntimeError
     * @throws SyntaxError
     */
    public function renderTemplateMail(Mail $mail, string $template, array $parameters = null): Mail
    {
        $rendered = $this->templating->render($template, $parameters);
        $mail->setHtmlBody($rendered);

        return $mail;
    }

    /**
     * @param Mail $mail
     *
     * @throws TransportExceptionInterface
     * @throws NoRecipientException
     */
    public function sendMail(Mail $mail)
    {
        if (!is_array($mail->getToEmail()) || empty($mail->getToEmail())) {
            throw new NoRecipientException();
        }

        $message = new Email();
        $message->from($mail->getFromMail());
        foreach ($mail->getToEmail() as $to) {
            $message->addTo($to);
        }
        $message->subject($mail->getSubject());
        if ($mail->getHtmlBody()) {
            $message->html($mail->getHtmlBody(), 'text/html');
        } else {
            $message->text($mail->getPlainBody(), 'text/plain');
        }
        $this->mailer->send($message);
    }
}
