<?php declare(strict_types=1);

namespace App\Exceptions\Controller;

use Exception;

/**
 *
 */
class TeamNotFoundException extends Exception
{
}
