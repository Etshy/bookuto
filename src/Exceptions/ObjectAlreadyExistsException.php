<?php
declare(strict_types=1);

namespace App\Exceptions;


use Exception;

/**
 * Class ObjectAlreadyExistsException
 * @package App\Exceptions
 */
class ObjectAlreadyExistsException extends Exception
{

}